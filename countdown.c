///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   Reference time: Fri Dec 31 23:59:58 3920
//
//   Years: 2020      Days: 31        Hours: 23       Minutes: 59     Seconds: 58
//   Years: 320913607         Days: -461158174        Hours: -1991745839      Minutes: -1991643855    Seconds: -98693133
//   Years: 320913607         Days: -461158174        Hours: -1991745839      Minutes: -1991643855    Seconds: -98693133
//   Years: 320913607         Days: -461158174        Hours: -1991745839      Minutes: -1991643855    Seconds: -98693133
//   Years: 320913607         Days: -461158174        Hours: -1991745839      Minutes: -1991643855    Seconds: -98693133
//   Years: 320913607         Days: -461158174        Hours: -1991745839      Minutes: -1991643855    Seconds: -98693133
//   Years: 320913607         Days: -461158174        Hours: -1991745839      Minutes: -1991643855    Seconds: -98693133
//
// @author Destynee Fagaragan <djaf6@hawaii.edu>
// @date   11/02/2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>


int main(int argc, char* argv[]) {
   // struct for time reference //
   struct tm ref_time;
      // ref for seconds //
      ref_time.tm_sec = 58;
      // ref for minutes in an hour //
      ref_time.tm_min = 59;
      // ref for hours in a day //
      ref_time.tm_hour = 23;
      // ref for days of the month // 
      ref_time.tm_mday = 31;
      // ref for months in a year //
      ref_time.tm_mon = 11;
      // ref for years since 1900 //
      ref_time.tm_year = 2020;
      // ref for days of the week //
      ref_time.tm_wday = 4;
      // ref for days of the year //
      ref_time.tm_yday = 365;
      // ref for daylight savings //
      ref_time.tm_isdst = 0;
   
   // time variables //
   struct tm *time_one;
   struct tm *time_diff;

   time_t time_onE = mktime(&ref_time);
   time_t time_two = mktime(&ref_time);
   time_t diff_time;
   
   // print reference time //
   printf("Reference time: ");
   printf("%s\n", asctime(&ref_time));
   printf("Years: %d\t Days: %d\t Hours: %d\t Minutes: %d\t Seconds: %d\n", ref_time.tm_year, ref_time.tm_mday, ref_time.tm_hour, ref_time.tm_min, ref_time.tm_sec);

   while(1){
      
      // increment time //
      time_onE = time_onE + 1;
      // difference between times given //
      diff_time = difftime(time_onE,time_two);
      // convert time //
      time_diff - localtime(&diff_time);
      // print time difference //
      printf("Years: %d\t Days: %d\t Hours: %d\t Minutes: %d\t Seconds: %d\n", time_diff->tm_year, time_diff->tm_mday, time_diff->tm_hour, time_diff->tm_min, time_diff->tm_sec);
      
      // delay //
      sleep (1);
   }
   return 0;
}
